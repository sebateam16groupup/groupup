/**
 * Controller for editing the profile
 */

var mongoose = require("mongoose");
var passport = require("passport");
var User = require("../models/User");
var path = require('path');

//Change Name

var profileController = {};

//go to Profile
profileController.goToProfile = function (req, res) {
    res.sendFile(path.resolve('login.html'), {user: req.user});
};


//Show all users on one page
profileController.list = function (req, res) {
    User.find({}).exec(function (err, users) {
        if (err) {
            {
                console.log("Error:", err);
                res.send(err);
            }
        } else {
            res.render("../public/user/index", {users: users});
        }
    });
};
//Show only one
profileController.show = function (req, res) {
    User.findOne({_id: req.params.id}).exec(function (err, user) {
        if (err) {
            {
                console.log("Error:", err);
                res.send(err);
            }
        } else {
            res.render("../public/user/show", {user: user});
        }
    });
};
// Edit User
profileController.edit = function (req, res) {
    User.findOne({_id: req.params.id}).exec(function (err, user) {
        if (err) {
            {
                console.log("Error:", err);
                res.send(err);
            }
        } else {
            res.sendFile("../public/user/edit.html", {user: user});
        }
    });
};
// update currently edited user
profileController.update = function (req, res) {
    User.findByIdAndUpdate(req.params.id, {$set: {name: req.body.name, surname: req.body.surname, username: req.body.username}}, {new : true}, function (err, user) {
        if (err) {
            console.log(err);
            res.render("../public/user/edit", {user: req.body});
        } else {
            res.redirect("/user/show/" + user._id);
        }
    });
};

// delete user by ID
profileController.delete = function (req, res) {
    User.remove({_id: req.params.id}, function (err) {
        if (err) {
            console.log("Error:", err);
            res.send(err);
        } else {
            console.log("User deleted!");
            res.redirect("/user");
        }
    });
};


// Frotend functionalities

(function () {
    var app = angular.module("app", []);
    app.controller("UserController", function () {
        this.user = User;
    });

});

module.exports = profileController;






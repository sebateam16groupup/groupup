/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var mongoose = require('mongoose');
var GroupJoinings = require('../models/GroupJoining');
var path = require('path');

var GroupJoiningController = {};

GroupJoiningController.joingroup = function (req, res) {

    GroupJoinings.create({
        groupRef: req.body.groupid,
        userRef: req.body.userid
    }, function (err, join) {
        if (err) {
            console.log("Error: " + err);
            res.send(err);
        } else {
            res.status(200).send();
        }
    });
};

// get all joinings for a specific group (=> then access each "memberRef" to get the members.)
GroupJoiningController.getAllJoiningsForCurrentGroup = function (req, res) {
    // query to filter for userID and only display joining for the specific user
    var query = {};
    query["groupRef"] = req.body.group_id;
    GroupJoinings.find(query, function (err, groupjoinings) {
        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            console.log("Error:", err);
            res.send(err);
        } else {
            res.json(groupjoinings); // return all found groupjoinings in JSON format
        }
    });
};

// get all joinings for current user
GroupJoiningController.getAllJoiningsForCurrentUser = function (req, res) {
    // query to filter for userID and only display joining for the specific user
    var query = {};
    query["userRef"] = req.user._id;
    GroupJoinings.find(query, function (err, groupjoinings) {
        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            console.log("Error:", err);
            res.send(err);
        } else {
            res.json(groupjoinings); // return all found groupjoinings in JSON format 
        }
    });
};

// get all joinings for a specific group
GroupJoiningController.getAllJoiningsForCurrentGroup = function (req, res) {
    // query to filter for userID and only display joining for the specific user
    var query = {};
    query["groupRef"] = req.body.group_id;
    GroupJoinings.find(query, function (err, groupjoinings) {
        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            console.log("Error:", err);
            res.send(err);
        } else {
            res.json(groupjoinings); // return all found groupjoinings in JSON format
        }
    });
};

// get joining with a specific ID
GroupJoiningController.getJoiningById = function (req, res) {
    GroupJoinings.findById(req.body.groupjoining_id, function (err, joining) {
        if (err) {
            console.log("Error:", err);
            res.send(err);
        } else {
            res.json(joining);
        }
    });
};

// get all groupjoinings
GroupJoiningController.getAllGroupJoinings = function (req, res) {
    GroupJoinings.find(function (err, groupjoinings) {
        if (err) {
            console.log("Error:", err);
            res.send(err);
        } else {
            res.json(groupjoinings); // return all found groupjoinings in JSON format
        }
    });
};


module.exports = GroupJoiningController;
var mongoose = require("mongoose");
var passport = require("passport");
var User = require("../models/User");
var path = require('path');


var userController = {};

// Restrict access to root page
userController.home = function (req, res) {
    res.sendFile(path.resolve('public/index.html'), {user: req.user});
};

// Go to registration page
userController.register = function (req, res) {
    res.sendFile(path.resolve('public/register.html'));
};


// Post registration
userController.doRegister = function (req, res) {
    var prepUniname = req.body.university;
    var universityname = prepUniname.substring(7);
    User.register(new User({username: req.body.username, name: req.body.name, password: req.body.password, university: universityname}), req.body.password, function (err, user) {
        if (err) {
            console.log("Error at register " + err.toString());
            return res.redirect('register.html');
        }
        passport.authenticate('local')(req, res, function () {
            res.redirect('/profile');
        });
    });
};

// Go to login page
userController.login = function (req, res) {
    //console.log("ROUTE TO LOGIN TRIGGERED");
    res.sendFile(path.resolve('public/login.html'));
};

// Post login
userController.doLogin = function (req, res) {
    passport.authenticate('local')(req, res, function () {
        //console.log("Authenticate done");
        res.redirect('/profile');
    });
};

//go to Profiel Page
userController.profile = function (req, res) {
    res.sendFile(path.resolve('public/myprofile_plain.html'));
};

// logout
userController.logout = function (req, res) {
    req.logout();
    res.redirect('/');
};

module.exports = userController;

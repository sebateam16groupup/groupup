
var mongoose = require("mongoose");
var Universities = require("../models/University");
var path = require('path');

var UniversityController = {};

UniversityController.getUniversitybyParameterID = function (req, res) {
    Universities.findById(req.params.id, function (err, uni) {
        if (err) {
            console.log("Error:", err);
            res.send(err);
        } else {
            res.json(uni);
        }
    });

};


UniversityController.getallUniversities = function (req, res) {
    Universities.find(function (err, unis) {
        if (err) {
            console.log("Error:", err);
            res.send(err);
        } else {
            res.json(unis); // return all found lectures in JSON format
        }
    });
};

UniversityController.createANewUniversity = function (req, res) {
    Universities.create({
        uniName: req.body.univName
    }, function (err) {
        if (err) {
            console.log("Note: - University probably already exists. No need to create.");
            res.status(204).send();
        } else {
            res.status(200).send();
        }
    });
};

module.exports = UniversityController;








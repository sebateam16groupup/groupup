var mongoose = require("mongoose");
var Interests = require("../models/Interest");
var path = require('path');


var InterestController = {};


InterestController.getInterestsofaUser = function (req, res) {
    var query = {};
    query["userRef"] = req.params.interests_Userid;
    Interests.find(query, function (err, interests) {
        if (err) {
            console.log("Error:", err);
            res.send(err);
        } else {
            res.json(interests); // return all found roles in JSON format

        }
    });
};

// INTERESTS
InterestController.getAllInterestsOfCurrentUser = function (req, res) {
    // use mongoose to get all interests in the database
    var query = {};
    query["userRef"] = req.user._id;

    Interests.find(query, function (err, interests) {
        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            res.send(err);
        } else {
            res.json(interests); // return all interests in JSON format
        }
    });
};
// create interests and send back all interests after creation
InterestController.createInterestforCurrentUser = function (req, res) {
    // create a interest, information comes from AJAX request from Angular
    var prepKey = req.body.text + req.user._id;
    console.log(prepKey);

    Interests.find({key: prepKey}, function (err, something) {
        if (!something.length) {
            Interests.create({
                text: req.body.text,
                userRef: req.user._id,
                key: prepKey
            }, function (err, interest) {
                if (err) {
                    res.send(err);
                } else {
                    // get and return all the interests after you create another

                    var query = {};
                    query["userRef"] = req.user._id;

                    Interests.find(query, function (err, interests) {
                        if (err) {
                            res.send(err);
                        } else {
                            res.json(interests);
                        }
                    });
                }
            });
        } else {
            console.log("Error occurred. The user already added this interest.");
        }

    });
};
// delete a interest
InterestController.deleteInterestforCurrentUser = function (req, res) {
    Interests.remove({
        _id: req.params.interest_id
    }, function (err, interests) {
        if (err) {
            res.send(err);
        } else {
            // get and return all the interests after you create another
            var query = {};
            query["userRef"] = req.user._id;

            Interests.find(query, function (err, interests) {
                if (err)
                    res.send(err);
                res.json(interests);
            });
        }
    });
};


InterestController.getAllInterests = function (req, res) {
    // use mongoose to get all roles in the database
    Interests.find(function (err, interests) {
        if (err) {
            res.send(err);
        } else {
            res.json(interests); // return all roles in JSON format
        }
    });
};

InterestController.getAllInterestsWithSpecificText = function (req, res) {
    var query = {};
    query["text"] = req.params.text;
    Interests.find(query, function (err, interests) {
        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            res.send(err);
        } else {
            res.json(interests); // return all found roles in JSON format
        }
    });
};


module.exports = InterestController;
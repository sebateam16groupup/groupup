/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var mongoose = require('mongoose');
var GroupRequests = require('../models/GroupRequest');
var path = require('path');

var GroupRequestController = {};

// get all groupinvitations
GroupRequestController.getAllGroupRequests = function (req, res) {
    GroupRequests.find(function (err, grouprequests) {
        if (err) {
            console.log("Error:", err);
            res.send(err);
        } else {
            res.json(grouprequests); // return all found grouprequests in JSON format
        }
    });
};

// get all requests for current user
GroupRequestController.getAllRequestsForCurrentUser = function (req, res) {
    // query to filter for userID and only display requests for the specific user
    var query = {};
    query["userRef"] = req.user._id;
    GroupRequests.find(query, function (err, grouprequests) {
        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            console.log("Error:", err);
            res.send(err);
        } else {
            res.json(grouprequests); // return all found grouprequests in JSON format
        }
    });
};

// get all requests for a specific group
GroupRequestController.getAllRequestsForCurrentGroup = function (req, res) {
    // query to filter for userID and only display requests for the specific user
    var query = {};
    query["groupRef"] = req.body.group_id;
    GroupRequests.find(query, function (err, grouprequests) {
        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            console.log("Error:", err);
            res.send(err);
        } else {
            res.json(grouprequests); // return all found grouprequests in JSON format
        }
    });
};

// get invitation with a specific ID
GroupRequestController.getRequestById = function (req, res) {
    GroupRequests.findById(req.body.grouprequest_id, function (err, request) {
        if (err) {
            console.log("Error:", err);
            res.send(err);
        } else {
            res.json(request);
        }
    });
};

module.exports = GroupRequestController;


var mongoose = require("mongoose");
var Lectures = require("../models/Lecture");
var path = require('path');

var LectureController = {};




LectureController.joinLecture = function (req, res) {
    var prepKey = req.body.seluniName + req.body.lectName + req.user._id;
    Lectures.create({
        userRef: req.user._id,
        lectureName: req.body.lectName,
        uniName: req.body.seluniName,
        key: prepKey
                // NO "stat" !!! <- only for non-userReferenced entries!
    }, function (err, lect) {
        if (err) {
            console.log("Error occurred. (User probably already attends this lecture.)");
            res.status(204).send();
        } else {
            Lectures.find(function (err, lectures) {
                if (err) {
                    console.log("Error:", err);
                    res.send(err);
                } else {
                    res.json(lectures);
                } // return all found lectures in JSON format
            });
        }
    });
};

LectureController.leaveLecture = function (req, res) {
    Lectures.remove({_id: req.params.id}, function (err, success) {
        if (err) {
            console.log("Error:", err);
            res.send(err);
        } else {
            // get and return all the user's remaining lectures after you delete another
            var query = {};
            query["userRef"] = req.user._id;
            Lectures.find(query, function (err, lectures) {
                if (err) {
                    console.log("Error:", err);
                    res.send(err);
                } else {
                    res.json(lectures);
                }
            });
        }
    });
};

LectureController.getallUsersofaLecture = function (req, res) {

    var query = {};
    query["lectureStat"] = false;
    query["lectureName"] = req.params.lectname;
    query["uniName"] = req.params.uniname;
    console.log(query);
    Lectures.find(query, function (err, lectureentries) {
        if (err) {
            console.log("Error:", err);
            res.send(err);
        } else {
            res.json(lectureentries);
        } // return all found lectures in JSON format
    });

}

LectureController.getspecificStaticUniLectures = function (req, res) {
    // query to filter for static lectures from a specific university
    var query = {};
    query["lectureStat"] = req.params.static;
    query["uniName"] = req.params.name;
    Lectures.find(query, function (err, lectures) {
        if (err) {
            console.log("Error:", err);
            res.send(err);
        } else {
            res.json(lectures);
        } // return all found lectures in JSON format
    });
};

LectureController.GetAllStatLectures = function (req, res) {
    // query to filter for userID and only display lectures of the specific user
    var query = {};
    query["lectureStat"] = true;
    Lectures.find(query, function (err, lectures) {
        if (err) {
            console.log("Error:", err);
            res.send(err);
        } else {
            res.json(lectures);
        } // return all found lectures in JSON format
    });
};
LectureController.getCurrentUsersLectures = function (req, res) {
    // query to filter for userID and only display lectures of the specific user
    var query = {};
    query["userRef"] = req.user._id;
    Lectures.find(query, function (err, lectures) {
        if (err) {
            console.log("Error:", err);
            res.send(err);
        } else {
            res.json(lectures);
        } // return all found lectures in JSON format
    });
};

LectureController.createANewLecture = function (req, res) {
    Lectures.create({
        lectureName: req.body.lectName,
        uniName: req.body.uniName,
        key: req.body.uniquekey,
        lectureStat: true
    }, function (err) {
        if (err) {
            console.log("Note: - Lecture probably already exists. No need to create.");
            res.status(204).send();
        } else {
            res.status(200).send();
        }
    });
};

LectureController.getSpecificLecturebyParamId = function (req, res) {

    Lectures.find({_id: req.params.id}, function (err, lectures) {
        if (err) {
            console.log("Error:", err);
            res.send(err);
        } else {
            res.json(lectures);
        } // return all found lectures in JSON format
    });
};

module.exports = LectureController;








var mongoose = require("mongoose");
var User = require("../models/User");
var path = require('path');


var UserController = {};

UserController.getAllUsers = function (req, res) {
    // use mongoose
    User.find(function (err, users) {
        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            console.log("Error:", err);
            res.send(err);
        } else {
            res.json(users);
        } // return all users in JSON format
    });
};

UserController.getCurrentUser = function (req, res) {
    // use mongoose
    User.findById(req.user._id, function (err, user) {
        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            console.log("Error:", err);
            res.send(err);
        } else {
            res.json(user);
        }// return the current user in JSON format
    });
};

UserController.getUserbyParameterId = function (req, res) {
    User.findById(req.params.id, function (err, user) {
        if (err) {
            console.log("Error:", err);
            res.send(err);
        } else {
            res.json(user);
        }
    });
};

UserController.getOtherUserbyParamterId = function (req, res) {
    User.findById(req.params.id, function (err, user) {
        if (err) {
            console.log("Error:", err);
            res.send(err);
        } else {
            res.sendFile(path.resolve('public/otherUsersProfile.html'));
        }

    });
};


// update user and send back user after update
UserController.updateCurrentUser = function (req, res) {
// update user, information comes from AJAX request from Angular
    //console.log("Req "+ JSON.stringify(req));
    console.log("Req.body: " + JSON.stringify(req.body));
    //console.log(req);
    //myUniID = req.params.interest_id
    //console.log("MYUNIID: " +JSON.stringify(req.body.uni));
    User.findByIdAndUpdate(req.user._id, {
        $set: {
            studyprogram: req.body.program,
            description: req.body.description,
            name: req.body.name
        }
    },
            function (err, user) {
                if (err) {
                    console.log("Error:", err);
                    res.send(err);
                }
            });

    // send updated user back
    User.findById(req.user._id,
            function (err, user) {
                if (err) {
                    console.log("Error:", err);
                    res.send(err);
                } else {
                    res.json(user);
                }
            });
};
module.exports = UserController;

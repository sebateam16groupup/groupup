/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var mongoose = require('mongoose');
var GroupInvitations = require('../models/GroupInvitation');
var path = require('path');

var GroupInvitationController = {};

GroupInvitationController.createInvitation = function (req, res) {
//var prepKey = req.body.seluniName + req.body.lectName + req.user._id;
    GroupInvitations.create({
        groupRef: req.body.groupRef,
        userRef: req.body.userRef,
        lectRef: req.body.lectRef
                //,key: prepKey
    }, function (err, group) {
        if (err) {
            res.send(err);
            //res.status(204).send();
        } else {
            GroupInvitations.find(function (err, groupinvitations) {
                if (err) {
                    console.log("Error:", err);
                    res.send(err);
                } else {
                    res.json(groupinvitations);
                } // return all found lectures in JSON format
            }
            );
        }
        ;
    }
    );
};

// delete an invitation
GroupInvitationController.deleteInvite = function (req, res) {
    GroupInvitations.remove({
        _id: req.params.id
    }, function (err, inv) {
        if (err) {
            console.log("Error:", err);
            res.send(err);
        } else {
            res.status(204).send();
        }
        ;
    });
};


// get all invitations for a specific group
GroupInvitationController.getAllInvitationsForCurrentGroup = function (req, res) {
    // query to filter for userID and only display invitations for the specific user
    var query = {};
    query["groupRef"] = req.body.group_id;
    GroupInvitations.find(query, function (err, groupinvitations) {
        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            console.log("Error:", err);
            res.send(err);
        } else {
            res.json(groupinvitations); // return all found groupinvitations in JSON format
        }
    });
};

// get all groupinvitations
GroupInvitationController.getAllGroupInvitations = function (req, res) {
    GroupInvitations.find(function (err, groupinvitations) {
        if (err) {
            console.log("Error:", err);
            res.send(err);
        } else {
            res.json(groupinvitations); // return all found groupinvitations in JSON format
        }
    });
};
// get all invitations for current user
GroupInvitationController.getAllInvitationsForCurrentUser = function (req, res) {
    console.log("BODY: " + JSON.stringify(req.body));
    console.log("User ID: " + JSON.stringify(req.user._id));
    console.log("User ID: " + req.user._id);
    // query to filter for userID and only display invitations for the specific user
    var query = {};
    query["userRef"] = req.user._id;
    GroupInvitations.find(query, function (err, groupinvitations) {
        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            console.log("Error:", err);
            res.send(err);
        } else {
            res.json(groupinvitations);
        }// return all found groupinvitations in JSON format
    });
};

// get invitation with a specific ID
GroupInvitationController.getInvitationById = function (req, res) {
    GroupInvitations.findById(req.body.invitation_id, function (err, invitation) {
        if (err) {
            console.log("Error:", err);
            res.send(err);
        } else {
            res.json(invitation);
        }
    });
};
module.exports = GroupInvitationController;
var mongoose = require('mongoose');
var User = require('../models/User');
var Groups = require('../models/Group');
var Lectures = require("../models/Lecture");
var path = require('path');
var ChatController = require('./ChatController')


var GroupController = {};


GroupController.getall = function (req, res) {
    Groups.find(function (err, groups) {
        if (err) {
            console.log("Error:", err);
            res.send(err);
        } else {
            res.json(groups); // return all found lectures in JSON format
        }
    });
};


GroupController.getCurrentUsersGroups = function (req, res) {
    //console.log("USERID: " + JSON.stringify(req.user._id));
    var filteredgroups = [];
    Groups.find(function (err, groups) {
        if (err) {
            console.log("Error:", err);
            res.send(err);
        } else {

            groups.forEach(function (item) {
                // for each group element in DB
                item.userRef.forEach(function (usRef) {
                    // for each userRef(id) in the userRef ARRAY of a group element
                    compare = JSON.stringify(usRef);
                    compare2 = JSON.stringify(req.user._id);
                    if (compare == compare2) {
                        filteredgroups.push(item);
                    }
                    ;
                });
            });

            res.send(filteredgroups);
        }


    });
};

GroupController.getGroupEdit = function (req, res) {
    res.sendFile(path.resolve('public/groupspace_edit.html'))
};


GroupController.getGroupByParamterId = function (req, res) {
    res.sendFile(path.resolve('public/groupspace.html'));
};

GroupController.getGroupContent = function (req, res) {
    Groups.findById(req.params.id, function (err, group) {
        if (err) {
            console.log("Error:", err);
            res.send(err);
        } else {
            res.send(group);
        }

    });
};

//create new Group
GroupController.createGroup = function (req, res) {

//CHECK IF ALREADY IN A GROUP FOR LECTURE

// var userInGroup = function(){
//   Groups.find({userRef : req.user._id, lectureRef : req.body.lecture._id}, function(err, groups){
//  if (err){
//    console.log(err);
//  }
//  else {
//   console.log(groups.length);
//   return groups;
// }
// })
// return groups;
// };
//
// var temp = userInGroup();
// console.log(temp);
// if (temp > 0){
//   console.log("User has existing group for lecture");
// } else {

    var groupNameTemp = req.body.groupName;
//console.log(req);
// console.log("create Group in GroupController reached");
    var newGroup = new Groups({
        groupname: req.body.groupName,
        admin: req.user._id,
        userRef: req.user._id,
        maxMembers: req.body.maxMembers,
        members: [req.user._id],
        lectureRef: req.body.lecture._id});
    newGroup.save(function (err, newGroup) {
        if (err) {
            console.log("Error in Create Group - new Group " + err);
        } else {
            console.log(newGroup);
            ChatController.createChat(newGroup);
            res.json(newGroup);
        }
    });

};

GroupController.isAdmin = function (req, res) {
    Groups.find({_id: req.params.id, admin: req.user._id}, function (err, group) {
        console.log(group);
        if (err) {
            console.log(err);
        } else if (group !== null) {
            res.send(200, {"result": false})
                    ;
        } else
            res.send(200, {"result": true})

    });

};

//Delete a Group by Groupname
GroupController.deleteGroup = function (req, res) {

    var isAdmin = function () {
        Groups.find({_id: req.params.id, admin: req.user._id}, function (err, group) {
            if (err) {
                console.log(err);
            } else
                return group;
        });
    };

    if (isAdmin() !== null) {
        Groups.findByIdAndRemove({_id: req.params.id}, function (err) {
            if (err) {
                console.log("Error occured while deleting " + req.body.groupname + " \n" + err)
            } else {
                console.log("group deleted");
                res.send("Group Deleted");
            }
        });
    } else {
        console.log("no Admin");
    }

};

//Update Group Details such as Lecture and maxMembers
GroupController.updateGroupDetails = function (req, res) {
    var query = {groupname: req.body.name};
    Groups.findOne(query, {$set: {
            maxMembers: req.body.maxMembers,
            lecture: req.body.lecture,
            groupDescr: req.body.groupDescr}},
            function (err, group) {
                if (err) {
                    console.log(err);
                } else {
                    res.JSON(group);
                }

            });
};

// +++ todo: might not be working, please review +++
GroupController.addMember = function (req, res) {
    var query = {groupname: req.body.groupname};
    Groups.findOne(query, {$set: {member: req.body.member}}, function (err, group) {
        if (err) {
            console.log(err);
        } else {
            res.JSON(group);
        }
    });
};

//show all Members of a certain Group (NOT WORKING)
GroupController.showMembers = function (req, res) {
    var members = [];
//var memberNames = [];

    console.log("SHOW MEMBERS REACHED");
    var query = {_id: req.params.id};
    Groups.findOne(query, function (err, group) {
        if (err) {
            console.log(err);
        } else {
            console.log("GROUP FOUND\n" + group.userRef);
            members = group.userRef;
            //console.log(members);
            var memberNames = getMemberNames(members);
        }

    })
    console.log(memberNames);
    console.log("MEMBER NAMES END " + memberNames);
    res.json(memberNames);
};

function getMemberNames(members) {

//  console.log(members);
    var memberNames = [];
    members.forEach(function (member) {

        User.findById(member, function (err, foundMember) {
            if (err) {
                console.log(err);
            } else {
                //console.log("MEMBER AT INDEX I  " + foundMember);
                console.log("MEMBER FOUND");
                memberNames.push(foundMember.name);
                console.log(memberNames);
            }
            //return memberNames;
        })

    });

    console.log("END OF GET NAMES" + memberNames)
    return memberNames;
}
;




// show a certain Group
GroupController.showGroup = function (req, res) {
    console.log(req);
    var query = {groupname: req.body.groupname};

    Groups.find(query, function (err, group) {
        if (err) {
            console.log("Error occured at GroupController.showGroup with " + err);
        } else {
            res.json(group);
        }
    });
};

// show all Groups for a certain Member
GroupController.showAllGroupsOfCurrentUser = function (req, res) {
    // query to filter user ID
    var query = {};
    query["userRef"] = req.user._id;
    Groups.find(query, function (err, groups) {
        if (err) {
            console.log("Error: ", err);
            res.send(err);
        } else {
            res.json(groups); // return all found groups
        }
    });
};

//Update Group Description
/*GroupController.updateGroupDescr = function (req, res) {
 var query = {groupname: req.body.name};
 Groups.findOne(query, {$set: {groupdescr: req.body.groupdescr}}, function (err, group) {
 
 if (err) {
 console.log(err);
 } else {
 res.JSON(group);
 }
 
 });
 };
 */

//Update GroupDescription!
GroupController.updateGroup = function (req, res) {
    console.log("BODY: " + JSON.stringify(req.body));
    Groups.findByIdAndUpdate(req.body.groupid, {
        $set: {
            maxMembers: req.body.maxMem,
            groupDescr: req.body.groupDescr
        }
    },
            function (err, group) {
                if (err) {
                    console.log("Error: ", err);
                    res.send(err);
                }
                res.json(group);
            });
};


/*UserController.updateCurrentUser = function (req, res) {
 // update user, information comes from AJAX request from Angular
 //console.log("Req "+ JSON.stringify(req));
 //console.log("Req "+ JSON.stringify(req.body));
 //console.log(req);
 myUniID = req.params.interest_id
 console.log("MYUNIID: " +JSON.stringify(req.body.uni));
 User.findByIdAndUpdate(req.user._id, {
 $set: {
 studyprogram: req.body.program,
 description: req.body.description,
 university: req.body.uni,
 name: req.body.name
 }
 },
 function (err, user) {
 if (err) {
 console.log("Error:", err);
 res.send(err);
 }
 })
 //console.log( JSON.stringify(req.body ));
 //console.log( req.body.description);
 User.findById(req.user._id,
 function (err, user) {
 if (err) {
 console.log("Error:", err);
 res.send(err);
 }
 console.log("USER:" + JSON.stringify(user))
 res.json(user);
 });
 };*/

module.exports = GroupController;

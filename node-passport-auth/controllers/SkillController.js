var mongoose = require("mongoose");
var Skills = require("../models/Skill");
var path = require('path');


var SkillController = {};

SkillController.getAllSkillsOfCurrentUser = function (req, res) {
    // query to filter for userID and only display skills for the specific user
    var query = {};
    query["userRef"] = req.user._id;
    Skills.find(query, function (err, skills) {
        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            console.log("Error:", err);
            res.send(err);
        } else {
            res.json(skills); // return all found skills in JSON format
        }
    });
};

// create skill and send all skills of the current user back (after creation)
SkillController.createSkillforCurrentUser = function (req, res) {
    // create a skill, information comes from AJAX request from Angular
    var prepKey = req.body.text + req.user._id;
    console.log(prepKey);

    Skills.find({key: prepKey}, function (err, something) {
        if (!something.length) {
            Skills.create({
                text: req.body.text,
                userRef: req.user._id,
                key: prepKey
            }, function (err, skill) {
                if (err) {
                    console.log("Error: " + err);
                    res.send(err);
                } else {
                    // get and return all the skill after you create another
                    var query = {};
                    query["userRef"] = req.user._id;
                    Skills.find(query, function (err, skills) {
                        if (err) {
                            console.log("Error:", err);
                            res.send(err);
                        } else {
                            res.json(skills);
                        }
                    });
                }
            });
        } else {
            console.log("Error occurred. The user already added this skill.");
        }
    });
};

// delete a skill of the current user
SkillController.deleteSkillforCurrentUser = function (req, res) {
    Skills.remove({
        _id: req.params.skill_id
    }, function (err, skill) {
        if (err) {
            console.log("Error:", err);
            res.send(err);
        } else {
            // get and return all the skills after you delete another

            var query = {};
            query["userRef"] = req.user._id;

            Skills.find(query, function (err, skills) {
                if (err) {
                    console.log("Error:", err);
                    res.send(err);
                }
                res.json(skills);
            });
        }
    });
};

SkillController.getAllSkills = function (req, res) {
    // use mongoose to get all skills in the database
    Skills.find(function (err, skills) {
        if (err) {
            console.log("Error:", err);
            res.send(err);
        } else {
            res.json(skills); // return all skills in JSON format
        }
    });
};

SkillController.getAllSkillsWithSpecificText = function (req, res) {
    var query = {};
    query["text"] = req.params.text;
    Skills.find(query, function (err, skills) {
        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            console.log("Error:", err);
            res.send(err);
        } else {
            res.json(skills); // return all found skills in JSON format
        }
    });
};

SkillController.getSkillsofaUser = function (req, res) {
    var query = {};
    query["userRef"] = req.params.skills_Userid;
    Skills.find(query, function (err, skills) {
        if (err) {
            console.log("Error:", err);
            res.send(err);
        } else {
            res.json(skills);  // return all found Skills in JSON format
        }
    });
};

module.exports = SkillController;
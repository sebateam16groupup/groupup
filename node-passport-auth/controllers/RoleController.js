var mongoose = require("mongoose");
var Roles = require("../models/Role");
var path = require('path');


var RoleController = {};

RoleController.getRolesofaUser = function (req, res) {
    var query = {};
    query["userRef"] = req.params.roles_Userid;
    Roles.find(query, function (err, roles) {
        if (err) {
            console.log("Error:", err);
            res.send(err);
        } else {
            res.json(roles); // return all found roles in JSON format
        }
    });
};

RoleController.getAllRolesOfCurrentUser = function (req, res) {
    // use mongoose to get all roles in the database

    var query = {};
    query["userRef"] = req.user._id;

    Roles.find(query, function (err, roles) {
        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            console.log("Error:", err);
            res.send(err);
        } else {
            res.json(roles); // return all roles in JSON format
        }
    });
};

// create role and send all roles of the current user back (after creation)
RoleController.createRoleforCurrentUser = function (req, res) {
// create a role, information comes from AJAX request from Angular
    var prepKey = req.body.text + req.user._id;
    console.log(prepKey);
    
    Roles.find({key: prepKey}, function (err, something) {
        if (!something.length) {
            Roles.create({
                text: req.body.text,
                userRef: req.user._id,
                key: prepKey
            }, function (err, role) {
                if (err) {
                    console.log("Error:", err);
                    res.send(err);
                } else {
                    // get and return all the roles after you create another
                    var query = {};
                    query["userRef"] = req.user._id;
                    Roles.find(query, function (err, roles) {
                        if (err) {
                            console.log("Error:", err);
                            res.send(err);
                        } else {
                            res.json(roles);
                        }
                    });
                }
            });
        } else {
            console.log("Error occurred. The user already added this role.");
        }
    })

};
// delete a role of the current user
RoleController.deleteRoleforCurrentUser = function (req, res) {
    Roles.remove({
        _id: req.params.role_id
    }, function (err, role) {
        if (err) {
            console.log("Error:", err);
            res.send(err);
        } else {
            // get and return all the roles after you create another        
            // query for specific UserID (TODO! current: string based)

            var query = {};
            query["userRef"] = req.user._id;
            Roles.find(query, function (err, roles) {
                if (err) {
                    console.log("Error:", err);
                    res.send(err);
                }
                res.json(roles);
            });
        }
    });
};
RoleController.getAllRoles = function (req, res) {
    // use mongoose to get all roles in the database
    Roles.find(function (err, roles) {
        if (err) {
            console.log("Error:", err);
            res.send(err);
        } else {
            res.json(roles); // return all roles in JSON format
        }
    });
};
RoleController.getAllRolesWithSpecificText = function (req, res) {
    var query = {};
    query["text"] = req.params.text;
    Roles.find(query, function (err, roles) {
        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            console.log("Error:", err);
            res.send(err);
        } else {

            res.json(roles); // return all found roles in JSON format
        }
    });
};
module.exports = RoleController;
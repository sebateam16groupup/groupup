var Chat = require('../models/Chat');
var Group = require('../models/Group');

var ChatController = {};


ChatController.createChat = function (req, res) {
    // console.log("CREATE CHAT REACHED");
    // console.log(req);
    //
    // console.log("FIND REACHED");
    var query = {};
    //query["groupname"]=req.groupname;
    Group.find({groupname: req.groupname}, function (err, group) {
        if (err) {
            console.log(err);
        } else {
            console.log(group._id);
            //console.log("FOUND GROUP\n" + JSON.stringify(group));
            var newChat = new Chat({
                content: [],
                // userRef: req.userRef,
                groupRef: group[0]._id,
                lectureRef: group[0].lectureRef
            });

            newChat.save(function (err, newChat) {
                if (err) {
                    console.log("Error in ChatController - createChat " + err);
                } else
                    console.log("CHAT CREATED\n" + newChat);
                //res.json(newChat);
            });


        }
    });

};

// get Chat for specifc group ID
ChatController.getChat = function (req, res) {
    var query = {};
    query["groupRef"] = req.params.id;
    //console.log("QUERY " + query);
    Chat.find(query, function (err, chat) {
        if (err) {
            res.send(err);
        } else {
            //console.log("CHAT FOUND \n" +chat);
            res.json(chat);
        }
    })
};

//get incoming message and save to conent array of chat
ChatController.saveChat = function (req, res) {
    console.log("SAVE CHAT REACHED");
    console.log(req.params.id);
    console.log(req.body);
    var query = {};
    query["groupRef"] = req.params.id;

// works
    Chat.findOneAndUpdate(query, {$push: {content: {author: req.user._id, authorName: req.user.name, body: req.body.content, date: req.body.date}}}, function (err, chat) {
        if (err) {
            console.log(err);
        } else {
            console.log("CHAT UPDATED\n" + chat);
            res.json(chat);
        }
    });

};

module.exports = ChatController;

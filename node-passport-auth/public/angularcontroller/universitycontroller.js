
myApp.controller('allUniversitiesController', function ($scope, $http) {
    console.log("Uni Controller init.")
    /*
    $scope.universities = [
        {name: 'Technical University Munich', value: 'TUM'},
        {name: 'Ludwig Maximilian University', value: 'LMU'}
    ];*/
    var req = {
        method: 'GET',
        url: '/api/universities'
    };
    $http(req)
            .success(function (data) {
                console.log(JSON.stringify(data));
                 $scope.universities = data;
                              })
            .error(function (data) {
                console.log('Error in the universitycontroller.js - init');
            }); 

/*
        $http.get('/api/universities')
        .success(function(data) {
            console.log(data);
            $scope.universities = data;

        })
        .error(function(data) {
            console.log('Error: in universitiescontroller.js - get all universities');
        }); */
    $scope.createNewUniversity = function () {
        var request = {
            method: 'POST',
            url: '/api/newUniversity',
            data: {
                univName: $scope.formDataUniName,
            }
        }
        $http(request)
                .success(function (data) {
                    $scope.universities = data;
                    $scope.formDataUniName = ""; // clear the form so our user is ready to enter another
                })
                .error(function (err) {
                    console.log('Error in the universitycontroller.js - createUni()');
                });
    };
    
    
    
    $scope.deleteSingleUni = function (delUni) {
        var deleterequest = {
            url: '/api/deleteUni/' + delUni._id,
            method: 'DELETE'
        };
        $http(deleterequest)
                .success(function (remainingunis) {
                    $scope.universities = remainingunis; 
                })
                .error(function (err) {
                    console.log('Error in the allLecturesController.js - leaveLecture() ');
                });

    }; 
});
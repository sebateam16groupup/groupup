myApp.controller('userInterestSearcher', function ($scope, $http) {

    $scope.searchforONEInterest = function () {
        $scope.allinterests = [];
        $scope.usersFound = [];
        var temp = "";
        // get: filter interests for match. 
        var request = {
            // request has to be pre-built in order to avoid an error ("Can't set headers after they are sent"). 
            // The error could be caused by the url-building!
            method: 'GET',
            url: '/api/allInterests/' + $scope.formInterestSearch.text
        };
        $http(request)
                .success(function (data) {
                    //console.log(JSON.stringify(data));
                    $scope.allinterests = data;
                    $scope.allinterests.forEach(function (entry) { // for each interest look up its UserRef
                        var request_innen = {
                            // request has to be pre-built in order to avoid an error ("Can't set headers after they are sent"). 
                            // The error could be caused by the url-building!
                            method: 'GET',
                            url: '/api/user/' + entry.userRef
                        };
                        $http(request_innen) //Looking up userRef 
                                .success(function (success) {

                                    // Got the user back from DB                                    
                                    temp = "The interest-tag |" + entry.text + "| is owned by:"; // Build String (interest)
                                    var x = success;
                                    x.tempStr = temp;
                                    $scope.usersFound.push(success);
                                })
                                .error(function (error) {
                                    console.log('An Error occured in the interestSearchController.js - search for one Interest - inner error');
                                });
                    });

                })
                .error(function (data) {
                    console.log('An Error occured in the interestSearchController.js - search for one Interest - outer error');
                });
        $scope.formInterestSearch = {};
    };



    $scope.getAllInterestsAndUsers = function () {
        $scope.allinterests = [];
        $scope.usersFound = [];
        var temp = "";
        $http.get('/api/allInterests/')
                .success(function (data) {
                    //console.log(JSON.stringify(data));
                    $scope.allinterests = data;
                    $scope.allinterests.forEach(function (entry) { // for each interest look up its UserRef
                        var request_innen = {
                            // request has to be pre-built in order to avoid an error ("Can't set headers after they are sent"). 
                            // The error would be caused by the url-building!
                            method: 'GET',
                            url: '/api/user/' + entry.userRef
                        };
                        $http(request_innen) //Looking up userRef 
                                .success(function (success) {
                                    // Got the user back from DB                                    
                                    temp = "The interest-tag |" + entry.text + "| is owned by:"; // Build String (Interest)
                                    var x = success;
                                    x.tempStr = temp;
                                    $scope.usersFound.push(success);
                                })
                                .error(function (error) {
                                    console.log('An Error occured in the interestSearchController.js - search for All Interest - inner error');
                                })
                    });

                })
                .error(function (data) {
                    console.log('An Error occured in the interestSearchController.js - search for All Interest - outer error');
                });
        $scope.formInterestSearch = {};
    };



});
myApp.controller('interestadder', function ($scope, $http) {
    
    // initialize: when landing on the page, get all interests and show them
    $http.get('/api/interests')
        .success(function(data) {
            $scope.interests = data;
            //console.log(data);
        })
        .error(function(data) {
            console.log('An Error occured in the interestcontroller.js - init');
        });

    // when submitting the add form, send the text to the node API
    $scope.createinterest = function() {
        //console.log("creating interests -1");
        $http.post('/api/interests', $scope.formDataInterest)
            .success(function(data) {
                $scope.formDataInterest = {}; // clear the form so our user is ready to enter another
                //$scope.interests = data;
            
                //console.log(JSON.stringify(data));
                //makes skills unique by skill text
                var uniqueInterests = UniqueArraybyId(data, "text");

                function UniqueArraybyId(collection, keyname) {
                    var output = [],
                            keys = [];

                    angular.forEach(collection, function (item) {
                        var key = item[keyname];
                        if (keys.indexOf(key) === -1) {
                            keys.push(key);
                            output.push(item);
                        }
                    });
                    return output;
                };


                console.log(JSON.stringify(uniqueInterests));
                $scope.interests = uniqueInterests;
            
        
        })
            .error(function(data) {
                console.log('An Error occured in the interestcontroller.js - createinterest');
            });
    };

    // delete a interest after checking it
    $scope.deleteinterest = function(id) {
        $http.delete('/api/interests/' + id)
            .success(function(data) {
                $scope.interests = data;
            })
            .error(function(data) {
                console.log('An Error occured in the interestcontroller.js - deleteinterest');
            });
    };
 
});






myApp.controller('searchControllerCentral', function ($scope, $http) {
    $scope.savedUni = {};
    $scope.userUniName = "";
    $scope.selectedlecture = "";
    $scope.lectureUsers = [];
    $scope.userUniName = "";
    $scope.selectedlectureObj = {};
    $scope.registeredUsers = "";

    $scope.mygroups = [];
    $scope.selectedgroupObj = {};
    

    $http.get('/groups/my')
            .success(function (data) {

                //data.forEach(function (item) {
                //if (item)
                //    $scope.mygroups.push(item);
                //});
                $scope.mygroups = data;
            })
            .error(function (data) {
                console.log('Error in searchControllerCentra.js - get my groups');
            });


    var getuserReq = {
        method: 'GET',
        url: '/api/user'
    };
    $http(getuserReq)
            .success(function (data) {
                $scope.userUniName = data.university;
                //get the static lectures of the user's university.
                console.log("My uni: " + $scope.userUniName);
                var getLectsrequest = {
                    method: 'GET',
                    url: '/api/uniLectures/' + $scope.userUniName + "/" + "true"
                };
                $http(getLectsrequest)
                        .success(function (data) {
                            //console.log(JSON.stringify((data)));
                            $scope.lectures = data;
                        })
                        .error(function (data) {
                            console.log('Error in searchControllerCentral.js - get group');
                        });
                // GET UNI END.            

            })
            .error(function (data) {
                console.log('Error in the searchControllerCentral.js - get current user profile');
            });




//########## FUNCTIONALITIES ############

    $scope.changedGroup = function (value) {
        // activate - show invite buttons
        $scope.selectedgroupObj = value;
        $scope.inviteButtonShow = true;
    };

    $scope.changedLecture = function (value) {
        $scope.VisibleCont0 = true;
        $scope.VisibleCont1 = true;
        $scope.VisibleCont2 = true;
        $scope.VisibleCont3 = true;
        $scope.VisibleCont4 = true;
        $scope.VisibleCont5 = true;

        
        $scope.usersFound = [];
        $scope.selectedlectureObj = value;
        $scope.lectureUsers = [];
        var getlectUsersrequest = {
            url: '/api/lectureusers/' + $scope.selectedlectureObj.lectureName + "/" + $scope.userUniName,
            method: 'GET'
        };
        $http(getlectUsersrequest)
                .success(function (data) {
                    data.forEach(function (item) {
                        $scope.lectureUsers.push(item.userRef);
                    });
                    console.log($scope.lectureUsers);

                    var usercountLecture = $scope.lectureUsers.length;
                    if (usercountLecture == 1) {
                        $scope.registeredUsers = "" + usercountLecture + " user is registered for this lecture.";

                    } else {
                        $scope.registeredUsers = "" + usercountLecture + " users are registered for this lecture.";

                    }
                })
                .error(function (data) {
                    console.log('An Error occured in the searchControllerCentral.js - get a lecture"s users');
                });

    };


    $scope.showallUsers = function () { 
        $scope.VisibleCont6 = false;
        $scope.usersFound = [];
        $scope.lectureUsers.forEach(function (entry) { // for each interest look up its UserRef
            var request_innen = {
                // request has to be pre-built in order to avoid an error ("Can't set headers after they are sent"). 
                // The error could be caused by the url-building!
                method: 'GET',
                url: '/api/user/' + entry
            };
            $http(request_innen) //Looking up userRef 
                    .success(function (success) {

                        // Got the user back from DB                                    
                        temp = "User: " + success.username; // Build String (interest)
                        var x = success;
                        x.tempStr = temp;
                        $scope.usersFound.push(success);
                    })
                    .error(function (error) {
                        console.log('An Error occured in the searchControllerCentral.js - search for one Interest - inner error');
                    });
        });



    };


    $scope.invitetoGroup = function (userelement) {
        console.log("USER " + userelement._id);
        console.log("LECTURE ID " + $scope.selectedlectureObj._id);
        console.log("GROUP ID " + $scope.selectedgroupObj._id);

        var inviterequest = {
            // request has to be pre-built in order to avoid an error ("Can't set headers after they are sent"). 
            // The error could be caused by the url-building!
            method: 'POST',
            url: '/creategroupInvitation',
            data: {
                groupRef: $scope.selectedgroupObj._id,
                userRef: userelement._id,
                lectRef: $scope.selectedlectureObj._id
            }
        };
        $http(inviterequest)
                .success(function (data) {
                    //console.log(data);
                })
                .error(function (data) {
                    console.log('An Error occured in the searchControllerCentral.js - invitetoGroup');
                });
    };


//############ Search functions ##############
    $scope.searchforONEInterest = function () {
        $scope.filterobjective = "preference: '" + $scope.formInterestSearch.text + "'.";
        $scope.resultString= 0;
        $scope.VisibleCont6 = true;
        $scope.allinterests = [];
        $scope.usersFound = [];
        var temp = "";
        // get: filter interests for match. 
        var request = {
            // request has to be pre-built in order to avoid an error ("Can't set headers after they are sent"). 
            // The error could be caused by the url-building!
            method: 'GET',
            url: '/api/allInterests/' + $scope.formInterestSearch.text
        };
        $http(request)
                .success(function (data) {
                    $scope.allinterests = data;
                    $scope.allinterests.forEach(function (entry) { // for each interest look up its UserRef
                        var request_innen = {
                            // request has to be pre-built in order to avoid an error ("Can't set headers after they are sent"). 
                            // The error could be caused by the url-building!
                            method: 'GET',
                            url: '/api/user/' + entry.userRef
                        };
                        $http(request_innen) //Looking up userRef 
                                .success(function (success) {

                                    // Got the user back from DB                                    
                                    temp = "The interest-tag |" + entry.text + "| is owned by:"; // Build String (interest)
                                    var x = success;
                                    x.tempStr = temp;
                                    if ($scope.lectureUsers.indexOf(success._id) > -1) { // nur wenn der user (seine id) in der Lecture_userID liste ist wird er angezeigt.
                                        $scope.usersFound.push(success);
                                        $scope.resultString =  $scope.usersFound.length;
                                    }
                                })
                                .error(function (error) {
                                    console.log('An Error occured in the searchControllerCentral.js - search for one Interest - inner error');
                                });
                    });

                })
                .error(function (data) {
                    console.log('An Error occured in the searchControllerCentral.js - search for one Interest - outer error');
                });
        $scope.formSkillSearch = {};
        $scope.formRoleSearch = {};
        
    };



    $scope.searchforONERole = function () {
        $scope.filterobjective = "role: '" + $scope.formRoleSearch.text + "'.";
        $scope.resultString= 0;
        $scope.VisibleCont6 = true;
        $scope.allroles = [];
        $scope.usersFound = [];
        var temp = "";
        // get: filter roles for match. 
        var request = {
            // request has to be pre-built in order to avoid an error ("Can't set headers after they are sent"). 
            // The error could be caused by the url-building!
            method: 'GET',
            url: '/api/allRoles/' + $scope.formRoleSearch.text
        };
        $http(request)
                .success(function (data) {
                    //console.log(JSON.stringify(data));
                    $scope.allroles = data;
                    $scope.allroles.forEach(function (entry) { // for each role look up its UserRef
                        var request_innen = {
                            // request has to be pre-built in order to avoid an error ("Can't set headers after they are sent"). 
                            // The error could be caused by the url-building inside of the $http.
                            method: 'GET',
                            url: '/api/user/' + entry.userRef
                        };
                        $http(request_innen) //Looking up userRef 
                                .success(function (success) {
                                    // Got the user back from DB                                    
                                    temp = "The role-tag |" + entry.text + "| is owned by:"; // Build String (Role + Username)
                                    var x = success;
                                    x.tempStr = temp;
                                    if ($scope.lectureUsers.indexOf(success._id) > -1) { // nur wenn der user (seine id) in der Lecture_userID liste ist wird er angezeigt.
                                        $scope.usersFound.push(success);
                                        $scope.resultString =  $scope.usersFound.length;
                                    }
                                })
                                .error(function (error) {
                                    console.log('An Error occured in the searchControllerCentral.js - search for one Roles - inner error');
                                });
                    });
                })
                .error(function (data) {
                    console.log('An Error occured in the searchControllerCentral.js - search for one Roles - outer error');
                });
        $scope.formSkillSearch = {};
        $scope.formInterestSearch = {};
    };


    $scope.searchforONESkill = function () {
        $scope.filterobjective = "skill: '" + $scope.formSkillSearch.text + "'.";
        $scope.resultString= 0;
        $scope.VisibleCont6 = true;
        $scope.allskills = [];
        $scope.usersFound = [];
        var temp = "";
        // get: filter skills for match. 
        var request = {
            // request has to be pre-built in order to avoid an error ("Can't set headers after they are sent"). 
            // The error could be caused by the url-building!
            method: 'GET',
            url: '/api/allSkills/' + $scope.formSkillSearch.text
        };
        $http(request)
                .success(function (data) {
                    //console.log(JSON.stringify(data));
                    $scope.allskills = data;
                    $scope.allskills.forEach(function (entry) { // for each skill look up its UserRef
                        var request_innen = {
                            // request has to be pre-built in order to avoid an error ("Can't set headers after they are sent"). 
                            // The error could be caused by the url-building!
                            method: 'GET',
                            url: '/api/user/' + entry.userRef
                        };
                        $http(request_innen) //Looking up userRef 
                                .success(function (success) {

                                    // Got the user back from DB                                    
                                    temp = "The skill-tag |" + entry.text + "| is owned by:"; // Build String (Skill + Username)
                                    var x = success;
                                    x.tempStr = temp;

                                    //console.log("INDEX: " +$scope.lectureUsers.indexOf(success._id));                           
                                    if ($scope.lectureUsers.indexOf(success._id) > -1) { // nur wenn der user (seine id) in der Lecture_userID liste ist wird er angezeigt.
                                        $scope.usersFound.push(success);
                                        $scope.resultString =  $scope.usersFound.length;
                                    }
                                })
                                .error(function (error) {
                                    console.log("An Error occured in the searchControllerCentral.js - search for one Skill - inner error");
                                })
                    });

                })
                .error(function (data) {
                    console.log('An Error occured in the searchControllerCentral.js - search for one Skill - outer error');
                });
        $scope.formRoleSearch = {};
        $scope.formInterestSearch = {};
    };




});
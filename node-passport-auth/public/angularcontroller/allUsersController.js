myApp.controller('allUsersController', function ($scope, $http) {

    $http.get('/api/allUsers')
        .success(function (data) {
          console.log(JSON.stringify(data)); 
            $scope.users = data;
            //$scope.filter = filter;
        })
        .error(function (data) {
            console.log('An Error occured in the allUsersController.js - init');
    });
});


myApp.controller('groupController', function ($scope, $http, $location) {
    console.log("groupController init.");


    // $scope.groupName = {}
    // $scope.lectureName = {}
    // $scope.groupMembers = {}
    $scope.currentuser = {};
    // $scope.groupDescription = {}


    $scope.changedLecture = function (value) {
        $scope.selectedlecture = value;
    };

    $scope.changedGroupName = function (value) {
        $scope.selectedGroupName = value;
    };


    $scope.changedMaxMembers = function (value) {
        $scope.selectedMaxMembers = value;
    };


//get current user
    $http.get('/api/user')
            .success(function (data) {

                $scope.currentuser = data;
                var urlpre = '/api/uniLectures/' + $scope.currentuser.university + "/true";
                // get the lectures of the current user's university
                $http.get(urlpre)
                        .success(function (data) {
                            $scope.lectures = data;
                        })
                        .error(function (data) {
                            console.log('Error in groupController.js - get user uni')
                        })


            })
            .error(function (data) {
                console.log('Error in groupController.js - get user');
            });



// Send information to backend
    $scope.createGroup = function () {

        var req = {
            method: 'POST',
            url: '/groups/create',
            data: {
                lecture: $scope.selectedlecture,
                groupName: $scope.selectedGroupName,
                maxMembers: $scope.selectedMaxMembers,

            }
        };
        //backend responses with the newly created group in response
        $http(req).success(function (response) {
            $scope.newgroupURL = "http://localhost:3000/groups/" + response._id;
            location.replace($scope.newgroupURL);


        });
        // these values are loaded after creating a group on the groupspace.html
        // +++ BEWARE: the values are erased after reload since they are only valid inside the .success bock +++

        // DELETED.

    };
// $http.get('/groups/show').success(function(data){
//       console.log(JSON.stringify(data));
//       $scope.groupName = data.groupName;
//       $scope.lectureName = data.lectureName;
//       $scope.groupMembers = data.members;
//       $scope.groupDescription = data.groupDescr;
// })
// .error (function(err){
//   console.log("error occured at angular/groupController/get/'groups/show'");
// });



    // $scope.addGroupDescr = function () {
    //     var req = {
    //         method: 'POST',
    //         url: 'api/group',
    //         data: {
    //             groupdescr : $scope.groupdescr
    //         }
    //     };
    //     $http(req)
    //             .success(function(data) {
    //                 $scope.savedGroupDescr = data.groupdescr;
    //     })
    //             .error(function (data) {
    //                 console.log('Error in groupController.js - addGroupDescr');
    //     });
    // };
    //
    //     $scope.deleteGroup = function(id) {
    //     $http.delete('/api/groups/' + id)
    //         .success(function(data) {
    //             $scope.groups = data;
    //             console.log(data);
    //         })
    //         .error(function(data) {
    //             console.log('Error: in groupController.js - deleteGroup()');
    //         });
    // };
    //
    // $scope.getAllGroupsOfCurrentUser = function () {
    //     $scope.allGroups = [];
    //     $scope.user = userRef;
    //     var temp = "";
    //     $http.get('/api/allGroups/')
    //             .success(function (data) {
    //                 //console.log(JSON.stringify(data));
    //                 $scope.allGroups = data;
    //                 $scope.allGroups.forEach(function (entry) {
    //                     var request_innen = {
    //                         // request has to be pre-built in order to avoid an error ("Can't set headers after they are sent").
    //                         // The error would be caused by the url-building!
    //                         method: 'GET',
    //                         url: '/api/user/' + entry.userRef
    //                     };
    //                     $http(request_innen) //Looking up userRef
    //                             .success(function (success) {
    //                                 // Got the user back from DB
    //                                 temp = "The Group |" + entry.text + "| is owned by:";
    //                                 var x = success;
    //                                 x.tempStr = temp;
    //                                 $scope.usersFound.push(success);
    //                             })
    //                             .error(function (error) {
    //                                 console.log('An Error occured in groupController.js - getAllGroupsOfCurrentUser inner');
    //                             });
    //                 });
    //
    //             })
    //             .error(function (data) {
    //                 console.log('An Error occured in groupController.js - getAllGroupsOfCurrentUser outer');
    //             });
    //     $scope.formSkillSearch = {};
    // };
    //


});

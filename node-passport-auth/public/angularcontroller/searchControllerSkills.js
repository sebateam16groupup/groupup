myApp.controller('userSkillSearcher', function ($scope, $http) {

    $scope.searchforONESkill = function () {
        $scope.allskills = [];
        $scope.usersFound = [];
        var temp = "";
        // get: filter skills for match. 
        var request = {
            // request has to be pre-built in order to avoid an error ("Can't set headers after they are sent"). 
            // The error could be caused by the url-building!
            method: 'GET',
            url: '/api/allSkills/' + $scope.formSkillSearch.text
        };
        $http(request)
                .success(function (data) {
                    //console.log(JSON.stringify(data));
                    $scope.allskills = data;
                    $scope.allskills.forEach(function (entry) { // for each skill look up its UserRef
                        var request_innen = {
                            // request has to be pre-built in order to avoid an error ("Can't set headers after they are sent"). 
                            // The error could be caused by the url-building!
                            method: 'GET',
                            url: '/api/user/' + entry.userRef
                        };
                        $http(request_innen) //Looking up userRef 
                                .success(function (success) {

                                    // Got the user back from DB                                    
                                    temp = "The skill-tag |" + entry.text + "| is owned by:"; // Build String (Skill + Username)
                                    var x = success;
                                    x.tempStr = temp;
                                    $scope.usersFound.push(success);
                                })
                                .error(function (error) {
                                    console.log("An Error occured in the skillSearchController.js - search for one Skill - inner error");
                                })
                    });

                })
                .error(function (data) {
                    console.log('An Error occured in the skillSearchController.js - search for one Skill - outer error');
                });
        $scope.formSkillSearch = {};
    };



    $scope.getAllSkillsAndUsers = function () {
        $scope.allskills = [];
        $scope.usersFound = [];
        var temp = "";
        $http.get('/api/allSkills/')
                .success(function (data) {
                    //console.log(JSON.stringify(data));
                    $scope.allskills = data;
                    $scope.allskills.forEach(function (entry) { // for each skill look up its UserRef
                        var request_innen = {
                            // request has to be pre-built in order to avoid an error ("Can't set headers after they are sent"). 
                            // The error would be caused by the url-building!
                            method: 'GET',
                            url: '/api/user/' + entry.userRef
                        };
                        $http(request_innen) //Looking up userRef 
                                .success(function (success) {
                                    // Got the user back from DB                                    
                                    temp = "The skill-tag |" + entry.text + "| is owned by:"; // Build String (Skill + Username)
                                    var x = success;
                                    x.tempStr = temp;
                                    $scope.usersFound.push(success);
                                })
                                .error(function (error) {
                                    console.log('An Error occured in the skillSearchController.js - search for all Skills - inner error');
                                })
                    });

                })
                .error(function (data) {
                    console.log('An Error occured in the skillSearchController.js - search for all Skills - outer error');
                });
        $scope.formSkillSearch = {};
    };



});
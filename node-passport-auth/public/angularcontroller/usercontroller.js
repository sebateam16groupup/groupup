myApp.controller('usercontroller', function ($scope, $http) {
    //console.log("usercontroller init.")
    $scope.NameBox = "";
    $scope.selectedprogram = "";
    $scope.studyprograms = ['Information Systems','Informatics','Bioinformatics','Something Else'];
    // when landing on the page, get all skills and show them */
    //get current user's profile
    var getuserReq = {
        method: 'GET',
        url: '/api/user'
    };
    $http(getuserReq)
            .success(function (data) {
                $scope.savedName = data.name;
                $scope.NameBox = data.name;
                $scope.savedUserName = data.username;
                $scope.saveduserDesc = data.description;

                $scope.selected_program = data.studyprogram;
                $scope.savedStudyProgramm = data.studyprogram;
                $scope.savedUniID = data.university;
                // $scope.selecteduniID = data.university; // keeps server from crashing if uni is not changed.
                $scope.formDataDescriptionX = data.description;

            })
            .error(function (data) {
                console.log('Error in the usercontroller.js - get current user profile');
            });


    $http.get('/groups/my')
            .success(function (data) {
                $scope.mygroups = data;
            }).error(function (data) {
        console.log('Error in the usercontroller.js - get current user groups');
    })


    //console.log("UNILALAL  " + $scope.savedUniID);
    $http.get('/api/mylectures')
            .success(function (data) {
                $scope.lectures = data;
            })
            .error(function (data) {
                console.log('Error in the usercontroller.js - get current user lectures');
            });


    //// THIS FUNCTION WILL BE MOVED/DELETED ONCE JOINING LECTURES IS ACTIVATED ////
    $scope.createLecture = function () {
        var requ = {
            method: 'POST',
            url: '/api/lecture/',
            data: {
                lectName: $scope.formDataLecture,
                uniName: $scope.selecteduni
                        //newlect:
            }
        };
        $http(requ)
                .success(function (data) {
                    $scope.formDataLecture = {}; // clear the form so our user is ready to enter another
                    $scope.lectures = data;
                })
                .error(function (err) {
                    console.log('Error in the usercontroller.js - createLecture(XXXX) ');
                });
    };
    //// END OF FUNCTION  ////
    /*
     $scope.changeduni = function (value) {
     $scope.selecteduniID = value._id;
     }*/


    $scope.updateUser = function () {
        console.log("NAMEBOX: " +$scope.NameBox);
        var myselect = (document.getElementById("testme").selectedIndex) -1;      
        var reqUser = {
            method: 'POST',
            url: '/api/user',
            data: {
                description: $scope.formDataDescriptionX,
                program: $scope.studyprograms[myselect],
                name: $scope.NameBox
            }
        };
        $http(reqUser)
                .success(function (data) {

                    $scope.formDataDescriptionX = data.description;
                    $scope.saveduserDesc = data.description;
                    $scope.savedStudyProgramm = data.studyprogram;
                    $scope.selectedprogram = data.studyprogram;
                    $scope.NameBox = data.name;

                    console.log(JSON.stringify(data));
                })
                .error(function (data) {
                    console.log('Error in the usercontroller.js - updateUser() ');
                });
    };
})/*
        .run(function (editableOptions) {
            editableOptions.theme = 'bs3';
        }); */
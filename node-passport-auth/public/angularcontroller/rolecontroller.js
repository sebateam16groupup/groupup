myApp.controller('roleadder', function ($scope, $http) {
    
    // initialize: when landing on the page, get all skills and show them
    $http.get('/api/roles')
        .success(function(data) {
            $scope.roles = data;
            //console.log(data);
        })
        .error(function(data) {
            console.log('An Error occured in the rolecontroller.js - init');
        });
    // when submitting the add form, send the text to the node API
    $scope.createRole = function() {
        $http.post('/api/roles', $scope.formDataRole)
            .success(function(data) {
                $scope.formDataRole = {}; // clear the form so our user is ready to enter another
               // $scope.roles = data;
            
                var uniqueRoles = UniqueArraybyId(data, "text");

                function UniqueArraybyId(collection, keyname) {
                    var output = [],
                            keys = [];

                    angular.forEach(collection, function (item) {
                        var key = item[keyname];
                        if (keys.indexOf(key) === -1) {
                            keys.push(key);
                            output.push(item);
                        }
                    });
                    return output;
                };


                console.log(JSON.stringify(uniqueRoles));
                $scope.roles = uniqueRoles;
            
            })
            .error(function(data) {
                console.log('An Error occured in the rolecontroller.js - createRole');
            });
    };

    // delete a todo after checking it
    $scope.deleteRole = function(id) {
        $http.delete('/api/roles/' + id)
            .success(function(data) {
                $scope.roles = data;
            })
            .error(function(data) {
                console.log('An Error occured in the rolecontroller.js - deleteRole');
            });
    };

 
});
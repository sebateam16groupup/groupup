myApp.controller('userRoleSearcher', function ($scope, $http) {

    $scope.searchforONERole = function () {
        $scope.allroles = [];
        $scope.usersFound = [];
        var temp = "";
        // get: filter roles for match. 
        var request = {
            // request has to be pre-built in order to avoid an error ("Can't set headers after they are sent"). 
            // The error could be caused by the url-building!
            method: 'GET',
            url: '/api/allRoles/' + $scope.formRoleSearch.text
        }
        $http(request)
                .success(function (data) {
                    //console.log(JSON.stringify(data));
                    $scope.allroles = data;
                    $scope.allroles.forEach(function (entry) { // for each role look up its UserRef
                        var request_innen = {
                            // request has to be pre-built in order to avoid an error ("Can't set headers after they are sent"). 
                            // The error could be caused by the url-building inside of the $http.
                            method: 'GET',
                            url: '/api/user/' + entry.userRef
                        };
                        $http(request_innen) //Looking up userRef 
                                .success(function (success) {
                                    // Got the user back from DB                                    
                                    temp = "The role-tag |" + entry.text + "| is owned by:"; // Build String (Role + Username)
                                    var x = success;
                                    x.tempStr = temp;
                                    $scope.usersFound.push(success);
                                })
                                .error(function (error) {
                                    console.log('An Error occured in the roleSearchController.js - search for one Roles - inner error');
                                })
                    });
                })
                .error(function (data) {
                    console.log('An Error occured in the roleSearchController.js - search for one Roles - outer error');
                });
        $scope.formRoleSearch = {};
    };



    $scope.getAllRolesAndUsers = function () {
        $scope.allroles = [];
        $scope.usersFound = [];
        var temp = "";
        $http.get('/api/allRoles/')
                .success(function (data) {
                    //console.log(JSON.stringify(data));
                    $scope.allroles = data;
                    $scope.allroles.forEach(function (entry) { // for each role look up its UserRef
                        var request = {
                            // request has to be pre-built in order to avoid an error ("Can't set headers after they are sent"). 
                            // The error would be caused by the url-building!
                            method: 'GET',
                            url: '/api/user/' + entry.userRef
                        };
                        $http(request) //Looking up userRef 
                                .success(function (success) {
                                    // Got the user back from DB                                   
                                    temp = "The role-tag |" + entry.text + "| is owned by:"; // Build String (Role + Username)
                                    var x = success;
                                    x.tempStr = temp;
                                    $scope.usersFound.push(success);
                                })
                                .error(function (error) {
                                    console.log('An Error occured in the roleSearchController.js - search for all Roles - inner error');
                                })
                    });
                })
                .error(function (data) {
                    console.log('An Error occured in the roleSearchController.js - search for all Roles - outer error');
                });
        $scope.formRoleSearch = {};
    };



});
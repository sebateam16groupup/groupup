


myApp.controller('xprepopulateMongo', function ($scope, $http) {
    console.log("xprepopulateMongo init.");

    $scope.TUM = {};
    $scope.LMU = {};
    var request = {
        method: 'POST',
        url: '/api/newUniversity',
        data: {
            univName: "LMU"
        }
    };
    $http(request);


    var request2 = {
        method: 'POST',
        url: '/api/newUniversity',
        data: {
            univName: "TUM"
        }
    };
    $http(request2);


    var intermediaryRequest = {
        method: 'GET',
        url: '/api/universities'
    };
    $http(intermediaryRequest)
            .success(function (data) {
                $scope.universities = data; // .uniName
                $scope.TUM = $scope.universities[0];
                $scope.LMU = $scope.universities[1];

            });


    var request3 = {
        method: 'POST',
        url: '/api/newlecture/',
        data: {
            lectName: "SEBA",
            uniName: "TUM",
            uniquekey : "TUMSEBA",
            newlect: true // Static Lecture, no userRef!
        }
    };
    $http(request3)
            .success(function (lects) {
            })
            .error(function (err) {
                console.log('Error in the xprepopulateUniversities.js - createNewLecture(1) ');
            });



    var request4 = {
        method: 'POST',
        url: '/api/newlecture/',
        data: {
            lectName: "Datenbanken 1",
            uniName: "TUM",
            uniquekey : "TUMDatenbanken1",
            newlect: true // Static Lecture, no userRef!
        }
    };
    $http(request4)
            .success(function (lects) {
            })
            .error(function (err) {
                console.log('Error in the xprepopulateUniversities.js - createNewLecture(1) ');
            });


    var request5 = {
        method: 'POST',
        url: '/api/newlecture/',
        data: {
            lectName: "Philosophy",
            uniName: "LMU",
            uniquekey : "LMUPhilosophy",
            newlect: true // Static Lecture, no userRef!
        }
    };
    $http(request5)
            .success(function (lects) {
            })
            .error(function (err) {
                console.log('Error in the xprepopulateUniversities.js - createNewLecture(1) ');
            });


    var request6 = {
        method: 'POST',
        url: '/api/newlecture/',
        data: {
            lectName: "Rechnungswesen",
            uniName: "LMU",
            uniquekey : "LMURechnungswesen",
            newlect: true // Static Lecture, no userRef!
        }
    };
    $http(request6)
            .success(function (lects) {
            })
            .error(function (err) {
                console.log('Error in the xprepopulateUniversities.js - createNewLecture(1) ');
            });
});
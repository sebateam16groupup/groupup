
/* global uniName */

myApp.controller('allLecturesController', function ($scope, $http) {
    //console.log("Lecture Controller init.");
    $scope.testString = "Join a lecture!";
    $scope.mylectureString = "My Lectures";
    $scope.userUni = "";
    // get the current's user Uni
    var getuserReq = {
        method: 'GET',
        url: '/api/user'
    };
    $http(getuserReq)
            .success(function (data) {
                $scope.userUni = data.university;
                console.log("UNI: " + JSON.stringify($scope.userUni));
                var req = {
                    url: '/api/uniLectures/' + $scope.userUni + "/" + true,
                    method: 'GET'
                };
                $http(req)
                        .success(function (data) {
                            $scope.lectures = data;
                        })
                        .error(function (err) {
                            console.log('Error in the allLecturesController.js - init 2');
                        });

            })
            .error(function (data) {
                console.log('Error in the allLecturesController.js - get current user profile');
            });



    // get the current user's lectures      
    var req = {
        method: 'GET',
        url: '/api/mylectures'
    };
    $http(req)

            .success(function (data) {
                $scope.mylectures = data;
            })
            .error(function (err) {
                console.log('Error in the allLecturesController.js - init 3');
            })



/////////////////// FUNCTIONS ///////////////////

    $scope.joinLecture = function (chosenlecture) {
        var request = {
            method: 'POST',
            url: '/api/joinLecture/',
            data: {
                lectName: chosenlecture.lectureName,
                seluniID: chosenlecture.uniRef,
                seluniName: chosenlecture.uniName
            }
        };
        $http(request)
                .success(function (lects) {
                    // after joining a lecture, refresh mylectures so the user does not have to refresh the page.
                    var req = {
                        method: 'GET',
                        url: '/api/mylectures'
                    };
                    $http(req)
                            .success(function (data) {
                                $scope.mylectures = data;
                            })
                            .error(function (err) {
                                console.log('Error in the allLecturesController.js - joinLecture - sub error.');
                            });
                })
                .error(function (err) {
                    console.log('Error in the allLecturesController.js - joinLecture() ');
                });

    };


    $scope.leaveLecture = function (leavLect) {
        var leaverequest = {
            url: '/api/leaveLecture/' + leavLect._id,
            method: 'DELETE'
        };
        $http(leaverequest)
                .success(function (mylects) {
                    $scope.mylectures = mylects;
                })
                .error(function (err) {
                    console.log('Error in the allLecturesController.js - leaveLecture() ');
                });

    };

});

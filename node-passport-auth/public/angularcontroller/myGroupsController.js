






myApp.controller('myGroupsController', function ($scope, $http, $location) {
    console.log("myGroupsController init.");

    $scope.invites = [];
    $scope.inviteGroups = [];
    $scope.inviteLectures = [];

    $scope.combinedArray = [];

    $http.get('/invitations/my')
            .success(function (data) {
                $scope.invites = data;
                $scope.invites.forEach(function (invite) {
                    // get the groupObject of each group which has invited the current user.
                    var url_prebuilt = "/api/group/" + invite.groupRef;
                    $http.get(url_prebuilt)
                            .success(function (data) {
                                $scope.inviteGroups.push(data);
                            })
                            .error(function (data) {
                                console.log('Error in groupController.js - get groups of invites');
                            });
                    // Get the lectures of those groups
                    var url_prebuilt2 = "/api/lecture/" + invite.lectRef;
                    $http.get(url_prebuilt2)
                            .success(function (data) {
                                $scope.inviteLectures.push(data[0]);
                            })
                            .error(function (data) {
                                console.log('Error in groupController.js - get groups of invites');
                            });

                });

                // '/api/group/:id'

                myAfterFunction();
            })
            .error(function (data) {
                console.log('Error in groupController.js - get group');

            });


    // get MY groups (for myGroups.Html)
    $http.get('/groups/my')
            .success(function (data) {
                $scope.mygroups = data;
            })
            .error(function (data) {
                console.log('Error in groupController.js - get group');
            });



    function myAfterFunction() {
        console.log('myAfterFunction');
        console.log("INVITE lectures: " + JSON.stringify($scope.inviteLectures));
        var x = 0;
        /*$scope.inviteGroups.forEach(function (invite) {        
         var building = [];
         building.push($scope.invite.groupname);
         building.push($scope.inviteLectures[x].lectureName);
         
         $scope.combinedArray.push(building);
         console.log("Block: " + JSON.stringify(building));
         var x = x + 1;
         });*/
        //console.log(JSON.stringify($scope.combinedArray));
    };

     $scope.joinGroupFunction = function(inv) {
         
         
         // PART 1 - delete invitation from DB
         /*
        console.log("got here.");
        console.log("Invite: " + JSON.stringify(inv));
        var prebuilt_url = "/deleteInvitation/" + inv._id;
        console.log(prebuilt_url);        
        $http.delete(prebuilt_url)
                .success(function (data) {   
                    console.log("Invitation deleted.");
            // worked.
                })
                .error(function (data) {
                    console.log('Error: in myGroupsController.js - joinGroupFunction()');
                });*/
         
         
        // PART 2 - add user to the memberList in the DB.
        
        
        var reqUser = {
            method: 'POST',
            url:  '/joingroup/',
            data: {
                groupid: inv.groupRef,
                userid: inv.userRef
            }
        };
        $http(reqUser)
                .success(function (data) {
                    console.log("SOMETHING CAME BACK: "+ JSON.stringify(data));
                })
                .error(function (data) {
                    console.log('Error in the myGroupsController.js - joinGroupFunction(Part 2) ');
                });
    };
    // build combined array.

    //$scope.invites = [];
    //$scope.inviteGroups = [];
    //$scope.inviteLectures = [];




});
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// um GruppenID dem Groupspace edit zu übergeben, sonst sind die Werte verloren
myApp.controller('groupEditController', function ($scope, $http, $location) {
    console.log("groupEditController init.");
    $scope.formDataGroupDescr = "";
    $scope.testString = "hello world!";

    var x = $location.absUrl();  // x = "http://localhost:3000/groups/5968cff7ebf8761f34a7786e".    
    var adress_groupID = x.substring(32); // deletes: "http://localhost:3000/groupedit/" and leaves id.    
    var request = {
        url: '/api/group/' + adress_groupID,
        method: 'GET'
    };
    $http(request)
            .success(function (data) {
                $scope.group = data;
                $scope.formDataGroupDescr = $scope.group.groupDescr;
                $scope.selectedMaxMembers = $scope.group.maxMembers;
            })
            .error(function (data) {
                console.log('Error: ' + data);
            });



    $scope.updatethisgroup = function () {
        console.log("DRESCR: " + $scope.formDataGroupDescr);
        var req = {
            method: 'POST',
            url: '/groups/updateGroup',
            data: {
                maxMem: $scope.selectedMaxMembers,
                groupDescr: $scope.formDataGroupDescr,
                groupid: $scope.group._id
            }
        };
        $http(req)
                .success(function (data) {
                    $scope.formDataGroupDescr = {};
                    $scope.groupDescription = data.groupDescr;
                })
                .error(function (data) {
                    console.log('Error in groupEditController.js - GroupDescr');
                });
    };

    /*  $scope.addGroupDescr = function () {
     console.log("ADDGROUPDESCR");
     console.log($scope.formDataGroupDescr);
     var req = {
     method: 'POST',
     url: '/groups/newDescr',
     data: {
     groupDescr : $scope.formDataGroupDescr,
     groupid : $scope.group._id
     }
     };
     $http(req)
     .success(function(data) {
     $scope.formDataGroupDescr.value = data.groupDescr;
     //               $scope.groupDescription = data.groupDescr;
     })
     .error(function(data) {
     console.log('Error in groupEditController.js - addGroupDescr');
     });
     };
     */

});
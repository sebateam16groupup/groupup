/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var filter;
myApp.controller('visitProfileController', function ($scope, $http, $location) {

    // initialize: get the user Object
    var x = $location.absUrl();  // x = "http://localhost:3000/593fa879675cd92060023b70".
    var adress_userID = x.substring(22); // deletes: "http://localhost:3000/" and leaves id.   
    var request = {
        method: 'GET',
        url: '/api/user/' + adress_userID
    };
    $http(request)
            .success(function (data) {
                // display User.
                $scope.savedUserName = data.username;
                $scope.NameBox = data.name;
                $scope.savedStudyProgramm = data.studyprogram;
                $scope.savedUni = data.university;
                $scope.saveduserDesc = data.description;
                $scope.userID = data._id;
            })
            .error(function (data) {
                console.log('Error: ' + data);
            });


    // initialize2: when landing on the page, get all roles
    var request2 = {
        method: 'GET',
        url: '/api/userRole/' + adress_userID
    }
    $http(request2)
            .success(function (data) {
                $scope.userRoles = data; // display roles            
            })
            .error(function (data) {
                console.log('An Error occured in the rolecontroller.js - init 2');
            });

    // initialize3: when landing on the page, get all skills
    var request3 = {
        method: 'GET',
        url: '/api/userSkill/' + adress_userID
    }
    $http(request3)
            .success(function (data) {
                $scope.userSkills = data; // display roles            
            })
            .error(function (data) {
                console.log('An Error occured in the rolecontroller.js - init 2');
            });

    // initialize3: when landing on the page, get all interests
    var request4 = {
        method: 'GET',
        url: '/api/userInterest/' + adress_userID
    }
    $http(request4)
            .success(function (data) {
                $scope.userInterests = data; // display roles            
            })
            .error(function (data) {
                console.log('An Error occured in the rolecontroller.js - init 2');
            });
});

myApp.controller('groupSpaceController', function ($scope, $http, $location, $interval) {
    console.log("groupSpaceController init.");
    $scope.grouplecture = "";

    var x = $location.absUrl();  // x = "http://localhost:3000/groups/5968cff7ebf8761f34a7786e".
    var adress_groupID = x.substring(29); // deletes: "http://localhost:3000/groups/" and leaves id.
    var request = {
        url: '/api/group/' + adress_groupID,
        method: 'GET'
    };
    $http(request)
            .success(function (data) {
                $scope.group = data;
                console.log(data);
                var requestinner = {
                    url: '/api/lecture/' + $scope.group.lectureRef,
                    method: 'GET'
                };
                $http(requestinner)
                        .success(function (data) {
                            $scope.grouplecture = data[0].lectureName;
                        })
                        .error(function (data) {
                            console.log('Error: ' + data);
                        });
            })
            .error(function (data) {
                console.log('Error: ' + data);
            });

    // initialize2: when landing on the page, get all roles
    var request2 = {
        method: 'GET',
        url: '/groups/members/' + adress_groupID
    };
    $http(request2)
            .success(function (data) {
                console.log(data);
                $scope.members = data; // display members
            })
            .error(function (data) {
                console.log('An Error occured in the groupSpaceController.js - init 2');
            });

    var getUserReq = {
        method: 'GET',
        url: '/groups/isAdmin/' + adress_groupID
    }
    $http(getUserReq).
            success(function (data) {
                console.log(data);
                $scope.admin = data;
            })

// ============== GROUP FEED =========================

// GET ALL MESSAGES FROM DB
//initialize3: get all stored messages on feed
    var request3 = {
        method: 'GET',
        url: '/groups/chat/' + adress_groupID
    };
    $scope.loadChat = $interval(function () {
        console.log(adress_groupID);
        $http(request3)
                .success(function (data) {
                    console.log(data);
                    // console.log(JSON.stringify((data)));
                    $scope.chats = data; // display chat messages
                })
                .error(function (data) {
                    console.log('An Error occured in the groupSpaceController.js - init 3');
                });
    }, 10000);




// SEND CHAT

    $scope.sendMessage = function (message) {
        console.log(message);
        var request4 = {
            method: 'POST',
            url: '/groups/chat/' + adress_groupID,
            data: {
                content: message,
                date: Date.now(),
                groupRef: adress_groupID
            } // EO DATA

        } // EO Request

        console.log(request4);

        $http(request4).success(function (response) {
            $scope.inputMessage = "";

            console.log(response);
        })
                .error(function (err) {
                    console.log(err);
                }); // EO HTTP

    } // EOF


    // get all members from DB to display in groupspace.html
    //   $http.get('/groups/members')
    //          .success(function (data) {
    //              console.log(JSON.stringify((data)));
    //             $scope.members = data;
    //         })
    //          .error(function (data) {
    //             console.log('Error in groupSpaceController.js - get members');
    //         });

    //update group
    $scope.updateGroupDetails = function () {
        var reqGroup = {
            method: 'POST',
            url: '/api/group/' + adress_groupID,
            data: {
                lecture: $scope.selectedlecture,
                maxMembers: $scope.selectedMaxMembers,
                //   members: $scope.addedMembers,
                groupDescr: $scope.groupDescr
            }
        };
        $http(reqGroup)
                .success(function (data) {
                    //        $scope.formDataDescriptionX.value = data.groupDescr;
                    $scope.groupDescr = data.groupDescr;
                    $scope.selectedlecture = data.lecture;
                    $scope.selectedmaxMembers = data.MaxMembers;
                    //        $scope.addedMembers = data.members;

                    //console.log(JSON.stringify(data));
                })
                .error(function (data) {
                    console.log('Error in the groupSpaceController.js - updateGroup() ');
                });
    };


});

//angular.module("myApp", ["ui.bootstrap"])

myApp.controller('skilladder', function ($scope, $http, $timeout) {
    // when landing on the own profile page, get all skills and show them
    $http.get('/api/skills')
            .success(function (data) {
                $scope.skills = data;
                //console.log(data);
            })
            .error(function (data) {
                console.log('Error: in skillcontroller.js - get current skill');
            });


    $http.get('/api/allSkills')
            .success(function (data) {
                //console.log(JSON.stringify(data));
                //makes skills unique by skill text
                var uniqueSkills = UniqueArraybyId(data, "text");

                function UniqueArraybyId(collection, keyname) {
                    var output = [],
                            keys = [];

                    angular.forEach(collection, function (item) {
                        var key = item[keyname];
                        if (keys.indexOf(key) === -1) {
                            keys.push(key);
                            output.push(item);
                        }
                    });
                    return output;
                };


                console.log(JSON.stringify(uniqueSkills));
                $scope.allSkills = uniqueSkills;
            })
            .error(function (data) {
                console.log('An Error occured in the allUsersController.js - init');
            });

    // when submitting the add form, send the text to the node API
    
    $scope.createSkill = function () {
        $http.post('/api/skills', $scope.formDataSkill)
                .success(function (data) {
                    $scope.formDataSkill = {}; // clear the form so our user is ready to enter another
                    $scope.skills = data;
                })
                .error(function (data) {
                    console.log('Error: in skillcontroller.js - createSkill');
                });
    };

    // delete a todo after checking it
    $scope.deleteSkill = function (id) {
        $http.delete('/api/skills/' + id)
                .success(function (data) {
                    $scope.skills = data;

                    numberOfSkills--;
                    document.getElementById('skillButton').disabled = false;

                    console.log(data);
                })
                .error(function (data) {
                    console.log('Error: in skillcontroller.js - deleteSkill()');
                });
    };


});
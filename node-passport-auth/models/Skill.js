
var mongoose = require('mongoose');

// define model =================
var Skill = mongoose.model('Skill', {
    text: {type: String},
    key : {type:String, unique: true},
    userRef : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'User'
    }
}); 


module.exports = Skill;
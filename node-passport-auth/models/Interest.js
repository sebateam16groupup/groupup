
var mongoose = require('mongoose');

// define model =================
var Interest = mongoose.model('Interest', {
    text: String,
    userRef : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'User'
    }
}); 


module.exports = Interest;
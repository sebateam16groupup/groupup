/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var GroupSchema = new Schema ({
    groupname : {type : String, required : true, unique : true},
    //chatID : {type : '_id', ref: 'ChatSchema'},
    lectureRef : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'Lecture'   },
    maxMembers : {type : Number},
    admin : {
      type : mongoose.Schema.Types.ObjectId,
      ref : 'User'
    }, 
    userRef : [{
        type : mongoose.Schema.Types.ObjectId,
        ref : 'User'

    }],
    lecture : {type : String},
    groupDescr : {type : String}
});

// exporting the scheme for usage outside usage
var Group = mongoose.model('Group', GroupSchema);
module.exports = Group;

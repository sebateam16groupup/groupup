

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var passportLocalMongoose = require('passport-local-mongoose');

// define model =================
//var Todo = mongoose.model('Todo', {
//    text: String
//});

//enhanced userSchema
var UserSchema = new Schema({
    description: String,
    username : {type :String, required : true, unique : true}, //serves as unique identifier
    password : {type : String, required: true},
    name : {type : String, required : true},
    surname : {type : String},
    created_at : {type: Date},
    updated_at : {type : Date},
    //roles : {type : 'roles', ref : 'RoletagSchema'},
    //skills : {type : 'skills', ref : 'SkilltagSchema'},
    //interests : {type : 'interests', ref : 'InteresttagSchema'},
    studyprogram : {type : String},
    personaldescription : {type : String},
    university : {type: String, required : true},    
    uniRef : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'University'
    }
    //lectures : {type : ['lecturename'], ref : 'LectureSchema'},
   // groups : {type : ['groupname'], ref : 'GroupSchema'} //todo: Change for later cross referencing with group schemes
});

UserSchema.plugin(passportLocalMongoose);

var User = mongoose.model('User', UserSchema);
module.exports = User;

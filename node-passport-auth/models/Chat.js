var mongoose = require('mongoose');

var ChatSchema = mongoose.Schema({
  content : [{
    author : {
      type : mongoose.Schema.Types.ObjectId,
      ref : 'User'
    },
    authorName : String, 
    body : String,
    date : Date
  }], // stores all the messages
  userRef : {
    type : mongoose.Schema.Types.ObjectId,
    ref : 'User'},
  groupRef : {
    type : mongoose.Schema.Types.ObjectId,
    ref : 'Group'
  }

});

var Chat = mongoose.model('Chat', ChatSchema);
module.exports = Chat;

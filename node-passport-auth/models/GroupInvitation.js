/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var GroupInvitationSchema = new Schema ({
    groupRef : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'Group'   
    },
    userRef : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'User'
    },
    lectRef : {
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Lecture' 
    }    
});

// exporting the scheme for usage outside usage
var GroupInvitation = mongoose.model('GroupInvitation', GroupInvitationSchema);
module.exports = GroupInvitation;



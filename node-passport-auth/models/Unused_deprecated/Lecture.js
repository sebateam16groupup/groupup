/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var LectureSchema = new Schema ({
    lecturename : {type : String, required : true, unique : true},
    lecturer : {type : String, required : true},
    chair : {type : String},
    participants : [{type : 'username', ref : 'UserSchema'}],
    university : [{type : 'uniname', ref : 'UniSchema'}]
});

// exporting the scheme for usage outside usage
var Lecture = mongoose.model('Lecture', LectureSchema);
module.exports = Lecture;


/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ChatSchema = new Schema ({
    _id : {type : mongoose.Schema.Types.ObjectId},
    taggedindex : {type : Number},
    messages : {type : [String]}
});

var Chat = mongoose.model('Chat', ChatSchema);
module.exports = Chat;
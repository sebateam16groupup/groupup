/**
 * Created by Dominik on 18.06.17.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


//Schema for Universities, currently
var UniSchema = new Schema({
    uniname : {type : String, required : true, unique : true},
    location : {type : String, required : true},
    studyprograms : [{type : String}]
});


// exporting the scheme for usage outside usage
var Uni = mongoose.model('Uni', UniSchema);
module.exports = Uni;

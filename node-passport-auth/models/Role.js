
var mongoose = require('mongoose');

// define model =================
var Role = mongoose.model('Role', {
    text: String,
    userRef : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'User'
    }
}); 


module.exports = Role;
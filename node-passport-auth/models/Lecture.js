

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var LectureSchema = new Schema ({
    lectureName : {type : String, required: true},
    key : {type:String, unique: true},
    userRef : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'User'
    },
    uniName: {type:String},
    lectureStat: {type: Boolean, default: false }
    
});

// exporting the scheme for usage outside usage
var Lecture = mongoose.model('Lecture', LectureSchema);
module.exports = Lecture;


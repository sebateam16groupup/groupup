var express = require('express');
var router = express.Router();
var profile = require("../controllers/ProfileController");

// IS THIS USED ??? --> ANSWER DG PLS 

// Get all users
router.get('/user', profile.list);

// Get single user by id
router.get('/user/show/:id', profile.show);

// Edit user
router.get('/user/edit.html/:id', profile.edit);

// Edit update
router.post('/user/update/:id', profile.update);

// Delete user
router.post('/user/delete/:id', profile.delete);


module.exports = router;

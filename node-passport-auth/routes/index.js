var express = require('express');
var router = express.Router();

var SkillController = require("../controllers/SkillController.js");
var RoleController = require("../controllers/RoleController.js");
var InterestController = require("../controllers/InterestController.js");

var auth = require("../controllers/AuthController.js");
var profile = require("../controllers/ProfileController.js");
var UserController = require("../controllers/UserController.js");
var LectureController = require("../controllers/LectureController.js");
var UniversityController = require("../controllers/UniversityController.js");
var GroupController = require('../controllers/GroupController.js');
var ChatController = require('../controllers/ChatController.js');
var GroupInvitationController = require('../controllers/GroupInvitationController.js');
var GroupJoiningController = require('../controllers/GroupJoiningController.js');
//checks if our users are logged in
    var isLoggedIn = function(req, res, next){
        if (req.isAuthenticated()){
          return next();
        }  else {
          res.sendFile('index.html')
        }
    };

// restrict index for logged in user only
router.get('/', auth.home);

//################ LOGIN/REGISTER/PROFILE ###############//
// route to register page
router.get('/register', auth.register);
// route for register action
router.post('/register', auth.doRegister);
// route to login page
router.get('/login', auth.login);
// route for login action
router.post('/login', auth.doLogin);
// route for logout action
router.get('/logout', auth.logout);
//route to profile_plain
router.get('/profile', isLoggedIn, auth.profile);


//################ SKILLS ###############//
//route to get all skills of the current user
router.get('/api/skills', isLoggedIn,SkillController.getAllSkillsOfCurrentUser);
//route to create new skill
router.post('/api/skills', isLoggedIn,SkillController.createSkillforCurrentUser);
//route ro delete specific Skill with provided id
router.delete('/api/skills/:skill_id', isLoggedIn,SkillController.deleteSkillforCurrentUser);

// route to get all skills in the database
router.get('/api/allSkills', isLoggedIn,SkillController.getAllSkills);
// route to get all skill-entries with a given text (paramter => text)
router.get('/api/allSkills/:text', isLoggedIn,SkillController.getAllSkillsWithSpecificText);

router.get('/api/userSkill/:skills_Userid', isLoggedIn,SkillController.getSkillsofaUser);

//################ ROLES ###############//
//route to get all roles of the current user
router.get('/api/roles', isLoggedIn,RoleController.getAllRolesOfCurrentUser);
//route to create new role
router.post('/api/roles', RoleController.createRoleforCurrentUser);
//route to delete specific role with provided id
router.delete('/api/roles/:role_id', isLoggedIn,RoleController.deleteRoleforCurrentUser);

// route to get all roles in the database
router.get('/api/allRoles/', isLoggedIn,RoleController.getAllRoles);
// route to get all role-entries with a given text (paramter => text)
router.get('/api/allRoles/:text', isLoggedIn,RoleController.getAllRolesWithSpecificText);
// route to get all Role Entries of a given user
router.get('/api/userRole/:roles_Userid', isLoggedIn,RoleController.getRolesofaUser);



//################ INTERESTS ###############//
//route to get all interests of the current user
router.get('/api/interests', isLoggedIn,InterestController.getAllInterestsOfCurrentUser);
//route to create new role
router.post('/api/interests', isLoggedIn,InterestController.createInterestforCurrentUser);
//route to delete specific role with provided id
router.delete('/api/interests/:interest_id', isLoggedIn,InterestController.deleteInterestforCurrentUser);

// route to get all skills in the database
router.get('/api/allInterests', isLoggedIn,InterestController.getAllInterests);
// route to get all skill-entries with a given text (paramter => text)
router.get('/api/allInterests/:text', isLoggedIn,InterestController.getAllInterestsWithSpecificText);
// route to get all Interest Entries of a given user
router.get('/api/userInterest/:interests_Userid', isLoggedIn,InterestController.getInterestsofaUser);


//################ USER ###############//
// route to get update current user object
router.post('/api/user', isLoggedIn,UserController.updateCurrentUser);
// route to get current user ojbect
router.get('/api/user', isLoggedIn,UserController.getCurrentUser);
// route to get all users objects
router.get('/api/allUsers', isLoggedIn,UserController.getAllUsers);
// route to get any user object by parameter(id)
router.get('/api/user/:id', isLoggedIn,UserController.getUserbyParameterId);
// route to visit another Users profile
router.get('/:id', isLoggedIn,UserController.getOtherUserbyParamterId);

//################ LECTURES ###############//
// route to get all Lectures which the current User attends.
router.get('/api/mylectures', isLoggedIn,LectureController.getCurrentUsersLectures);
// get all Users of a specific lecture
router.get('/api/lectureusers/:lectname/:uniname', LectureController.getallUsersofaLecture);
// create a new Lecture (static!)
router.post('/api/newlecture/', LectureController.createANewLecture);
// get lectures (ONLY STAT! <- without user-References)
router.get('/api/lectures', isLoggedIn,LectureController.GetAllStatLectures);
// join a lecture
router.post('/api/joinLecture/', isLoggedIn,LectureController.joinLecture);
// leave a lecture
router.delete('/api/leaveLecture/:id', isLoggedIn,LectureController.leaveLecture);
// get all available lectures of a specific university
router.get('/api/uniLectures/:name/:static', isLoggedIn,LectureController.getspecificStaticUniLectures );
// get specific lecture by ID
router.get('/api/lecture/:id', isLoggedIn,LectureController.getSpecificLecturebyParamId);


//################ UNIVERSITIES ###############//
// route to get all universities
router.get('/api/universities', UniversityController.getallUniversities);
// create a new university
router.post('/api/newUniversity', UniversityController.createANewUniversity);
// get current user's uni
router.get('/api/oneuniversity/:id', isLoggedIn,UniversityController.getUniversitybyParameterID);
// delete a university (admin)
//router.delete('/api/deleteUni/:id', isLoggedIn,UniversityController.deleteUniversity);

//################ GROUPS ###############//
// POST
router.post('/groups/create', isLoggedIn,GroupController.createGroup);

router.post('/groups/find', isLoggedIn,GroupController.showGroup);

//router.post('/groups/update', isLoggedIn,GroupController.updateGroupDetails);
router.post('/groups/updateGroup', isLoggedIn, GroupController.updateGroup);

router.post('/groups/delete', isLoggedIn,GroupController.deleteGroup);

router.post('/groups/addMember/:id', isLoggedIn,GroupController.addMember);


// GET
router.get('/groups/showGroup', isLoggedIn,GroupController.showGroup);

router.get('/groups/members/:id', isLoggedIn,GroupController.showMembers);


router.get('/groups/my', isLoggedIn,GroupController.getCurrentUsersGroups);
// route to get all groups
router.get('/groups/all', isLoggedIn,GroupController.getall);
// route to get to the edit groupspace (&id in url shown)
router.get('/groupedit/:id', isLoggedIn, GroupController.getGroupEdit)
// route to redirect to groupSpace
router.get('/groups/:id', isLoggedIn,GroupController.getGroupByParamterId);
// route to get group itself (and its contents!)
router.get('/api/group/:id', isLoggedIn,GroupController.getGroupContent);

router.get('/groups/chat/:id', isLoggedIn, ChatController.getChat);

router.post('/groups/chat/:id', isLoggedIn, ChatController.saveChat);

router.get('groups/isAdmin/:id', isLoggedIn, GroupController.isAdmin);



//############ GROUP INVITATIONS ###########
// create a new Invitation
router.post('/creategroupInvitation', isLoggedIn, GroupInvitationController.createInvitation);
// get Invitations of current user
router.get('/invitations/my', isLoggedIn, GroupInvitationController.getAllInvitationsForCurrentUser);
// delete Invitation
router.delete('/deleteInvitation/:id', isLoggedIn, GroupInvitationController.deleteInvite)

// ########## Group Joinings ###############
router.post('/joingroup/', isLoggedIn, GroupJoiningController.joingroup);

// application -------------------------------------------------------------
router.get('*', function (req, res) {
    res.sendFile('./public/index.html'); // load the single view file (angular will handle the page changes on the front-end)
});



module.exports = router;
